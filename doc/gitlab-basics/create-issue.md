---
redirect_to: '../user/project/issues/index.md#view-and-manage-issues'
---

This document was moved to [another location](../user/project/issues/index.md#view-and-manage-issues).

<!-- This redirect file can be deleted after February 1, 2021. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->
